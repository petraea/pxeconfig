# Copyright (C) 2021
#
# This file is part of the pxeconfig utils
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
#
"""
Author: Bas van der Vlies <bas.vandervlies@surf.nl>
Date  : 12 February 2002
Desc. : This script is used to control how a node is booted with PXE
        enabled network cards. The node boots and fetch a pxe
        config file which tells how the node must boot. This daemon
        enables a client to remove his/here pxe config file. With the
        next boot it will use the default one.

        command line option:
          -V/--version
            Prints the version number and exits

        Note:
          This server can ONLY be started from inetd/xinetd.
"""
import sys, os, time
from subprocess import Popen, PIPE
import socket
import syslog
import getopt

# PXE modules
from pxe_global import *

# DEBUG
#
DEBUG=1

# Some Global Constants
#
BUFSIZE=4096
SHORTOPT_LIST='V'
LONGOPT_LIST=['version']

PXE_CONF_DIR = '/tftpboot/pxelinux.cfg'
UEFI_CONF_DIR = '/tftpboot/uefi'

VERSION = '5.0.0'

# Give the current time
#
def now():
    return time.ctime(time.time())

def arp_check(arp_cmd, ip):
    ##
    # No ARP command set, just return
    #
    result = False
    if not arp_cmd:
        return result

    else:
        ##
        #  arp -n 192.168.146.112
        # Address          HWtype  HWaddress           Flags Mask Iface
        # 192.168.146.112  ether   00:23:ae:fd:cf:74   C          vlan133

        arp_command = arp_cmd.split(' ')
        proc = Popen(arp_command + [ip],stdout=PIPE)
        lines,_ = proc.communicate()
        lines = lines.decode().strip().split('\n')
        for line in lines:
            if line.startswith(ip):
                t = line.split()
                mac_addr = t[2]
                haddr = '01-{}'.format(mac_addr.replace(':', '-').lower())

                pxe_file = os.path.join(PXE_CONF_DIR, haddr)

                uefi_file = "grub.cfg-{}".format(haddr)
                uefi_file = os.path.join(UEFI_CONF_DIR, uefi_file)

                if DEBUG:
                    print("pxe_file = {}, uefi_file = {}".format(pxe_file, uefi_file))

                if os.path.exists(pxe_file):
                    remove_link(pxe_file)
                    result = True

                if os.path.exists(uefi_file):
                    remove_link(uefi_file)
                    result = True
    return result

def remove_link(filename):
    """
    Remove the link
    """
    try:
        os.unlink(filename)
        syslog.openlog("pxeconfigd")
        syslog.syslog(syslog.LOG_INFO, filename)
        syslog.closelog()
    except OSError:
        err_msg = "No permission to remove file: {}".format(filename)
        sys.stderr.write(err_msg)
        sys.exit(1)

def remove_links(ip, arp_cmd):
    """
    This removes the pxe config filename for the host that is connected:
        ip      : ip address of the client host
        arp_cmd : For Dynamic ips, look up the mac address via arp
    """

    skip_arp_check = False

    ## translate ip address ---> hex address
    #
    d = ip.split('.')
    haddr = '{:02X}{:02X}{:02X}{:02X}'.format(int(d[0]), int(d[1]), int(d[2]), int(d[3]))

    pxe_file = os.path.join(PXE_CONF_DIR, haddr)

    uefi_file = "grub.cfg-{}".format(haddr)
    uefi_file = os.path.join(UEFI_CONF_DIR, uefi_file)

    if DEBUG:
        print("pxe_file = {}, uefi_file = {}".format(pxe_file, uefi_file))

    if os.path.exists(pxe_file):
        skip_arp_check = True
        remove_link(pxe_file)

    if os.path.exists(uefi_file):
        skip_arp_check = True
        remove_link(uefi_file)

    if not skip_arp_check:
        arp_check(arp_cmd, ip)


# This function handles the client connection. It closes
# the connection if there is no data
#
def handleConnection(settings):
    """
    Determines which host connects to the server
    """

    # Determine client address
    #
    try:
        client_addr = socket.fromfd(sys.stdin.fileno(), socket.AF_INET, socket.SOCK_STREAM)
        client_ip = client_addr.getpeername()[0]
    except socket.error as detail:
        error =  "pxeconfigd can only be started from inetd!!!"
        raise PxeConfig(error)


    if DEBUG:
        print('ip = {}'.format(client_ip))

    try:
        cmd = '{} {}'.format(settings['daemon_script_hook'], client_ip)
        run_command(None, cmd)
    except KeyError:
        pass

    try:
        arp_cmd = settings['arp_command']
    except KeyError:
        arp_cmd = None

    remove_links(client_ip, arp_cmd)
    sys.exit(0)

def check_args(argv, settings):
        """
        Must we use another directory for the PXE configuration
        """
        global PXE_CONF_DIR
        global UEFI_CONF_DIR

        try:
            opts, args = getopt.getopt(argv[1:], SHORTOPT_LIST, LONGOPT_LIST)
        except getopt.error as detail:
            print(__doc__)
            print(detail)
            sys.exit(1)

        for opt, value in opts:
            if opt in ['-V', '--version']:
                print(VERSION)
                sys.exit(0)

        try:
            PXE_CONF_DIR = settings['pxe_config_dir']
        except KeyError:
            pass

        try:
            UEFI_CONF_DIR = settings['uefi_config_dir']
        except KeyError:
            pass

        PXE_CONF_DIR = os.path.realpath(PXE_CONF_DIR)
        UEFI_CONF_DIR = os.path.realpath(UEFI_CONF_DIR)

        if not os.path.isdir(PXE_CONF_DIR):
            PXE_CONF_DIR = None

        if not os.path.isdir(PXE_CONF_DIR):
            UEFI_CONF_DIR = None

        if not ( PXE_CONF_DIR or UEFI_CONF_DIR ):
            error =  'No boot directory defined for PXE or UEFI'
            raise PxeConfig(error)

def server():
    """
    Start the daemon
    """
    parser_config, default_settings = ReadConfig()
    check_args(sys.argv, default_settings)
    handleConnection(default_settings)

if __name__ == '__main__':
    server()
